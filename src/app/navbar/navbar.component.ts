import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user:boolean = false;
  constructor() { }

  ngOnInit() {
    
      if (localStorage.getItem('User'))
      {
        this.user = true;
      }
      else
      {
        this.user= false;
      }
  }
  logout ()
  {
      localStorage.removeItem('User');
      this.user = false;
  }

}
