import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-information-user',
  templateUrl: './information-user.component.html',
  styleUrls: ['./information-user.component.css']
})
export class InformationUserComponent implements OnInit {
  User : any = null;
  constructor() { }

  ngOnInit() {
    this.User = JSON.parse(localStorage.getItem('User'));
  }

}
