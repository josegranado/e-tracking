import { Component, OnInit } from '@angular/core';
import { OrderService} from '../services/order.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  user: any = null;
  formOrder: boolean = false;
  constructor( orderService: OrderService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('User'));
  }
  createOrder(): void
  {
    this.formOrder = true;
  }
  regresar():void
  {
    this.formOrder = false;
  }
}
