import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map-orders-view',
  templateUrl: './map-orders-view.component.html',
  styleUrls: ['./map-orders-view.component.css']
})
export class MapOrdersViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.loadScript();
  }

  public loadScript() {
    let body = <HTMLDivElement> document.body;
    let script = document.createElement('script');
    script.innerHTML = '';
    script.src = 'assets/map-view-order.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);
  }
}
