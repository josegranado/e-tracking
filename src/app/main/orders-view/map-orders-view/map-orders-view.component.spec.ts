import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapOrdersViewComponent } from './map-orders-view.component';

describe('MapOrdersViewComponent', () => {
  let component: MapOrdersViewComponent;
  let fixture: ComponentFixture<MapOrdersViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapOrdersViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapOrdersViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
