import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../services/order.service';
import { Order } from '../../interfaces/order';
@Component({
  selector: 'app-orders-view',
  templateUrl: './orders-view.component.html',
  styleUrls: ['./orders-view.component.css']
})
export class OrdersViewComponent implements OnInit {
  orders : Order[] = null;
  nElements : number = 0;
  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.orderService.index().subscribe( (data: Order[]) => {this.orders = data});
    this.nElements = 0;
  }

}
