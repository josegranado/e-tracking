import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../services/order.service';
@Component({
  selector: 'app-form-register-order',
  templateUrl: './form-register-order.component.html',
  styleUrls: ['./form-register-order.component.css']
})
export class FormRegisterOrderComponent implements OnInit {
  orderCreated: boolean = false;
  coordenadaMsg: boolean = false;
  strMsg: boolean = false;
  coordenadas: any = 
  {
    latitud: null,
    longitud: null
  }
  order: any =
  {
    peso: null,
    descripcion: null,
    direccion:
    {
      descripcion: null,
      linea_1: null,
      linea_2: null,
      ciudad: null,
      pais: null,
      codigo_postal: null
    },
    destinatario:
    {
      nombre: null,
      apellido: null
    },
    coordenada: {
      latitud: null,
      longitud: null,
    }
  };
  constructor(private orderService: OrderService) { }

  ngOnInit() {
    setTimeout(() => 
    {
      this.order.coordenada.latitud = (<HTMLInputElement>document.getElementById('latitudFormOrder')).value;
      this.order.coordenada.longitud = (<HTMLInputElement>document.getElementById('longitudFormOrder')).value;
    },500);
    
  }
  sendOrder (): void
  {
    if ( (this.order.direccion.descripcion.length <= 150) && (this.order.descripcion.length <= 150))
    {
      this.orderService.save(this.order).subscribe((data) => this.orderCreated = true);
    }
    else
    {
      this.strMsg = true;
    }
  }
  guardarCoordenadas () : void
  {
    this.order.coordenada.latitud = (<HTMLInputElement>document.getElementById('latitudFormOrder')).value;
    this.order.coordenada.longitud = (<HTMLInputElement>document.getElementById('longitudFormOrder')).value;
    this.coordenadaMsg = true;
  }
}
