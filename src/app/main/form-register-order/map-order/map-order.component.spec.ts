import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapOrderComponent } from './map-order.component';

describe('MapOrderComponent', () => {
  let component: MapOrderComponent;
  let fixture: ComponentFixture<MapOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
