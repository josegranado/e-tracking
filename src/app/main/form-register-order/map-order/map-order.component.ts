import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map-order',
  templateUrl: './map-order.component.html',
  styleUrls: ['./map-order.component.css']
})
export class MapOrderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.loadScript();
  }
  public loadScript() {
    let body = <HTMLDivElement> document.body;
    let script = document.createElement('script');
    script.innerHTML = '';
    script.src = 'assets/map-order.js';
    script.async = true;
    script.defer = true;
    body.appendChild(script);
  }

}
