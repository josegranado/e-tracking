import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRegisterOrderComponent } from './form-register-order.component';

describe('FormRegisterOrderComponent', () => {
  let component: FormRegisterOrderComponent;
  let fixture: ComponentFixture<FormRegisterOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRegisterOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRegisterOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
