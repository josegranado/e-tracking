import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../interfaces/user'; 
@Injectable({
  providedIn: 'root'
})
export class UserService {
  API_ENDPOINT = 'https://e-tracking.herokuapp.com/api/v1';
  Request = null;
  constructor( private httpClient: HttpClient) { }

  save( user: User)
  {
    const headers = new HttpHeaders({'Content-Type':'application/json'}); 
    return this.httpClient.post(this.API_ENDPOINT+'/users/create', user, { headers: headers});
  }

  login ( userLogin: any)
  {
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    this.Request = this.httpClient.post(this.API_ENDPOINT+'/login',userLogin,{ headers: headers});
    return this.Request;
  }
}
