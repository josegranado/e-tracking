import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  API_ENDPOINT = 'https://e-tracking.herokuapp.com/api/v1';
  user: any = null;
  id: number = null;
  constructor(private httpClient: HttpClient) { }
  index ()
  {
    const headers = new HttpHeaders({'Content-Type':'application/json'}); 
    this.user = JSON.parse(localStorage.getItem('User'));
    this.id = this.user.id;
    return this.httpClient.get(this.API_ENDPOINT+'/users/'+this.id+'/orders', { headers: headers});
  }
  save ( order: any)
  {
    const headers = new HttpHeaders({'Content-Type':'application/json'}); 
    this.user = JSON.parse(localStorage.getItem('User'));
    this.id = this.user.id;
    return this.httpClient.post(this.API_ENDPOINT+'/users/'+this.id+'/orders/create', order, { headers: headers});
  }
}
