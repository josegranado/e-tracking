import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  UserLogin:any = 
  {
    email: null,
    password: null
  };
  data:any = {};
  Message:boolean = false;
  Form:boolean = true;
  constructor( private userService: UserService, private router: Router) { }
  
  ngOnInit() {
  }
  login ()
  {
    this.userService.login(this.UserLogin).subscribe( (data) => { 
      this.data = data;   
      this.Form = false; 
      localStorage.setItem('User', JSON.stringify(this.data));
      this.router.navigate(['/']);
      location.reload();
    }, (error) => { 
      this.Message = true
    });
  }
}
