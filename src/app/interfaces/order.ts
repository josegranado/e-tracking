export interface Order
{
    id: number,
    descripcion: string,
    direccion_id: number,
    coordenadas_id: number,
    destinatario_id: number,
    direccion?:any,
    coordenada?:any,
    destinatario?:any,
    created_at?: string,
    updated_at?:string
}