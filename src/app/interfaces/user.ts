export interface User
{
    nombre: string,
    apellido: string,
    email: string,
    fecha_nacimiento: string,
    password: string,
    ordenes?: any
}