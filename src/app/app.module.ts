import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SlideshowComponent } from './home/slideshow/slideshow.component';
import { InformacionComponent } from './home/informacion/informacion.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MapComponent } from './home/map/map.component';
import { FooterComponent } from './home/footer/footer.component';
import { RegisterComponent } from './register/register.component';
import { FormRegisterComponent } from './register/form-register/form-register.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MainComponent } from './main/main.component';
import { FormRegisterOrderComponent } from './main/form-register-order/form-register-order.component';
import { MapOrderComponent } from './main/form-register-order/map-order/map-order.component';
import { InformationUserComponent } from './main/information-user/information-user.component';
import { OrdersViewComponent } from './main/orders-view/orders-view.component';
import { MapOrdersViewComponent } from './main/orders-view/map-orders-view/map-orders-view.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    SlideshowComponent,
    InformacionComponent,
    LoginComponent,
    MapComponent,
    FooterComponent,
    RegisterComponent,
    FormRegisterComponent,
    MainComponent,
    FormRegisterOrderComponent,
    MapOrderComponent,
    InformationUserComponent,
    OrdersViewComponent,
    MapOrdersViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
