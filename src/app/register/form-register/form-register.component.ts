import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../interfaces/user';
@Component({
  selector: 'app-form-register',
  templateUrl: './form-register.component.html',
  styleUrls: ['./form-register.component.css']
})
export class FormRegisterComponent implements OnInit {
  user: User = 
  {
    nombre: null,
    apellido: null,
    fecha_nacimiento: null,
    email: null,
    password: null
  };
  secondPassword : string = null;
  anio:string = null;
  regex:any = null;
  Form:boolean = true;
  Message:boolean = false;
  MessagePassword: boolean = false;
  MessageFecha: boolean = false;
  MessagePasswordRepeat: boolean = false;
  constructor(private UserService: UserService) { 
   
  }

  ngOnInit() {
  }

  registrarUsuario()
  {
    this.regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;

    if ( this.regex.test(this.user.password))
    {
      this.anio = this.user.fecha_nacimiento.split('-')[0];
      if ( parseInt(this.anio) < 2001)
      {
        if (this.user.password == this.secondPassword)
        {
          this.UserService.save(this.user).subscribe( (data) => {alert('Usuario Creado')}, (error) => {console.log(error)});
          this.Message = true;
          this.Form = false;
        }
        else
        {
          this.MessagePasswordRepeat = true;
        }
      }
      else
      {
        this.MessageFecha = true;
      }
    }
    else
    {
      this.MessagePassword = true;
    }
    
    
    /*.subscribe( (data) => {
      alert('Pelicula guardada');
      console.log(data);
    }, (error) => {
      console.log(error);
    })*/
  }
}
