$(document).ready(function ()
{
    L.mapquest.key = 'TtwwGj4iZfEfulRWXXJ8WIJFT3kkIy4Y';
    latitudFormOrder = $('#latitudFormOrder');
    longitudFormOrder = $('#longitudFormOrder');
    // 'map' refers to a <div> element with the ID map
    //6.4237499, -66.5897293
    latitud = 6.4237499;
    longitud = -66.5897293;
    var map = L.mapquest.map('map', {
    center: [latitud, longitud],
    layers: L.mapquest.tileLayer('map'),
    zoom: 5
    });
    map.on('click', function(e) {
        loadMap(e.latlng.lat, e.latlng.lng);
        console.log(e.latlng)
    });
    var marker = L.marker([latitud, longitud]).addTo(map);
    latitudFormOrder.val(latitud).trigger('input');
    longitudFormOrder.val(longitud).trigger('input');
    $('#save-map').click(function () {
        loadMap(latitudFormOrder.val(),longitudFormOrder.val());
    }); 
    
    function loadMap (lat, lng)
    {
        marker.remove();
        latitudFormOrder.val(lat).trigger('input');
        longitudFormOrder.val(lng).trigger('input');
        marker = L.marker([lat, lng]).addTo(map); 
    }
});