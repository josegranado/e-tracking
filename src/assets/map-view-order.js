$(document).ready(function ()
{
    L.mapquest.key = 'TtwwGj4iZfEfulRWXXJ8WIJFT3kkIy4Y';
    latitud = 6.4237499;
    longitud = -66.5897293;
    var map = L.mapquest.map('map', {
        center: [latitud, longitud],
        layers: L.mapquest.tileLayer('map'),
        zoom: 2
    });
    let nFilas = 0
    setTimeout(function () {
        nFilas = $('#orders-views tr').length - 1;
        var ordersId = document.getElementsByClassName("orderId");
        var coordenadas = document.getElementsByClassName("coordenadas");
        nFilas = coordenadas.length;
        for (i = 0; i < coordenadas.length; i++)
        {
            ordersId[i].setAttribute('id','order-id-'+i);
            coordenadas[i].setAttribute('id', 'order-'+i);
        }
        contarOrders();
    } ,500);

    function contarOrders ()
    {
        for (i = 0; i < nFilas; i++)
        {
            order = 'order-'+i;
            orderId = 'order-id-'+i;
            trOrderId = document.getElementById(orderId);
            trOrder = document.getElementById(order);
            trOrderIdValue = trOrderId.textContent;
            value = trOrder.textContent;
            if ( value ){
            value= value.split(',');
            latitud = value[0].split('[');
            longitud = value[1].split(']');
            latitud = latitud[1];
            longitud = longitud[0];
            }
            agregarMarker(latitud, longitud, trOrderIdValue);
        }
    };
    
    function agregarMarker(latitud, longitud, id)
    {
        marker = L.marker([latitud, longitud]).addTo(map); 
        marker.bindPopup(`Order #${id} [${latitud}, ${longitud}]`).openPopup();
    }
});